console.log("Hello World");
function getSum(a,b){
	let sum = a + b;
	console.log("Displayed sum of " + a + " and " + b);
	console.log(sum);
}

function getDifference(a,b){
	let difference = a - b;
	console.log("Displayed difference of " + a + " and " + b);
	console.log(difference);
}

function getProduct(a,b){
	let product = a * b;
	return product
}

function getQuotient(a,b){
	let quotient = a / b;
	return quotient
}

function getAreaCircle(a){
	let areaCircle = 3.14159 * (a*a);
	return areaCircle;
}

function getAverage(a,b,c,d){
	let average = (a + b + c + d)/4;
	return average;
}

function isPass(a,b){
	let c = a / b;
	let isPassed = c >= 0.75;
	return isPassed;
}
getSum(5,15);
getDifference(20,5);
let product = getProduct(50,10);
let quotient = getQuotient(50,10);
console.log("The product of 50 and 10:")
console.log(product);
console.log("The quotient of 50 and 10:")
console.log(quotient);
let circleArea = getAreaCircle(15);
console.log("The result of getting the area of a circle with 15 radius:")
console.log(circleArea);
let averageVar = getAverage(20,40,60,80);
console.log("The average of 20,40,60 and 80:");
console.log(averageVar);
let isPassingScore = isPass(38,50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);